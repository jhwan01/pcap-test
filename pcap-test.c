#include <pcap.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <netinet/in.h>
#include <bits/endian.h>

#if __BYTE_ORDER == __LITTLE_ENDIAN
# define LIBNET_LIL_ENDIAN  1
#else
# define LIBNET_BIG_ENDIAN  1
#endif


#define ETHERNET_SIZE 14
#define ETHER_ADDR_LEN 6

struct libnet_ethernet_hdr
{
    u_int8_t  ether_dhost[ETHER_ADDR_LEN];/* destination ethernet address */
    u_int8_t  ether_shost[ETHER_ADDR_LEN];/* source ethernet address */
    u_int16_t ether_type;                 /* protocol */
};

struct libnet_ipv4_hdr
{
#if (LIBNET_LIL_ENDIAN)
    uint8_t ip_hl:4,      /* header length */
           ip_v:4;         /* version */
#endif
#if (LIBNET_BIG_ENDIAN)
    uint8_t ip_v:4,       /* version */
           ip_hl:4;        /* header length */
#endif
    u_int8_t ip_tos;       /* type of service */
#ifndef IPTOS_LOWDELAY
#define IPTOS_LOWDELAY      0x10
#endif
#ifndef IPTOS_THROUGHPUT
#define IPTOS_THROUGHPUT    0x08
#endif
#ifndef IPTOS_RELIABILITY
#define IPTOS_RELIABILITY   0x04
#endif
#ifndef IPTOS_LOWCOST
#define IPTOS_LOWCOST       0x02
#endif
    u_int16_t ip_len;         /* total length */
    u_int16_t ip_id;          /* identification */
    u_int16_t ip_off;
#ifndef IP_RF
#define IP_RF 0x8000        /* reserved fragment flag */
#endif
#ifndef IP_DF
#define IP_DF 0x4000        /* dont fragment flag */
#endif
#ifndef IP_MF
#define IP_MF 0x2000        /* more fragments flag */
#endif 
#ifndef IP_OFFMASK
#define IP_OFFMASK 0x1fff   /* mask for fragmenting bits */
#endif
    u_int8_t ip_ttl;          /* time to live */
    u_int8_t ip_p;            /* protocol */
    u_int16_t ip_sum;         /* checksum */
    struct in_addr ip_src, ip_dst; /* source and dest address */
};

struct libnet_tcp_hdr
{
    u_int16_t th_sport;       /* source port */
    u_int16_t th_dport;       /* destination port */
    u_int32_t th_seq;          /* sequence number */
    u_int32_t th_ack;          /* acknowledgement number */
#if (LIBNET_LIL_ENDIAN)
    u_int8_t th_x2:4,         /* (unused) */
           th_off:4;        /* data offset */
#endif
#if (LIBNET_BIG_ENDIAN)
    u_int8_t th_off:4,        /* data offset */
           th_x2:4;         /* (unused) */
#endif
    u_int8_t  th_flags;       /* control flags */
#ifndef TH_FIN
#define TH_FIN    0x01      /* finished send data */
#endif
#ifndef TH_SYN
#define TH_SYN    0x02      /* synchronize sequence numbers */
#endif
#ifndef TH_RST
#define TH_RST    0x04      /* reset the connection */
#endif
#ifndef TH_PUSH
#define TH_PUSH   0x08      /* push data to the app layer */
#endif
#ifndef TH_ACK
#define TH_ACK    0x10      /* acknowledge */
#endif
#ifndef TH_URG
#define TH_URG    0x20      /* urgent! */
#endif
#ifndef TH_ECE
#define TH_ECE    0x40
#endif
#ifndef TH_CWR
#define TH_CWR    0x80
#endif
    u_int16_t th_win;         /* window */
    u_int16_t th_sum;         /* checksum */
    u_int16_t th_urp;         /* urgent pointer */
};


void usage() {
	printf("syntax: pcap-test <interface>\n");
	printf("sample: pcap-test wlan0\n");
}

typedef struct {
	char* dev_;
} Param;

Param param = {
	.dev_ = NULL
};

bool parse(Param* param, int argc, char* argv[]) {
	if (argc != 2) {
		usage();
		return false ;
	}
	param->dev_ = argv[1];
	return true ;
}

int main(int argc, char* argv[]) {
	if (!parse(&param, argc, argv))
		return (-1) ;

	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* pcap = pcap_open_live(param.dev_, BUFSIZ, 1, 1000, errbuf);
	if (pcap == NULL) {
		fprintf(stderr, "pcap_open_live(%s) return null - %s\n", param.dev_, errbuf);
		return (-1) ;
	}

	while (true) {
		struct		libnet_ethernet_hdr *ethernet;
	        struct		libnet_ipv4_hdr *ip;
        	struct		libnet_tcp_hdr *tcp;
		struct		pcap_pkthdr* header;
		const u_char*	packet;
		int		res;
		bpf_u_int32	packet_check;
		
		res = pcap_next_ex(pcap, &header, &packet);
		ethernet = (struct libnet_ethernet_hdr*)packet;
		ip = (struct libnet_ipv4_hdr*)(packet + ETHERNET_SIZE);
		tcp = (struct libnet_tcp_hdr*)(packet + ETHERNET_SIZE + ip->ip_hl * 4);
		packet_check = ETHERNET_SIZE + ip->ip_hl * 4 + tcp->th_off * 4;
		packet += packet_check;
		if (res == 0) continue ;
		if (res == PCAP_ERROR || res == PCAP_ERROR_BREAK) {
			printf("pcap return %d(%s)\n", res, pcap_geterr(pcap));
			break ;
		}
		if (ntohs(ethernet->ether_type) != 2048) continue ;
                //ether_type = 0x0800은 IP version 4의 ethernet을 의미
                if (ip->ip_p != 6) continue ;
                //ip protocol = 6은 TCP의 IP protocol을 의미
                //위 두가지 조건을 모두 통과한다는 것은 TCP 형식의 파일을 의미 -> 출력
		
		printf("%u bytes captured\n", header->caplen);
		
		printf("Ethernet II, ");
		printf("SRC: (%02x:%02x:%02x:%02x:%02x:%02x), ", 
			ethernet->ether_shost[0],
			ethernet->ether_shost[1],
			ethernet->ether_shost[2],
			ethernet->ether_shost[3],
			ethernet->ether_shost[4],
			ethernet->ether_shost[5]);
		printf("DST: (%02x:%02x:%02x:%02x:%02x:%02x)\n",
			ethernet->ether_dhost[0],
			ethernet->ether_dhost[1],
			ethernet->ether_dhost[2],
			ethernet->ether_dhost[3],
			ethernet->ether_dhost[4],
			ethernet->ether_dhost[5]);

		printf("Internet Protocol Version 4, ");
		printf("Src: %s, ", inet_ntoa(ip->ip_src));
		printf("Dst; %s\n", inet_ntoa(ip->ip_dst));

		printf("Transmission Control Protocol, ");
		printf("Src Port: %d, ", ntohs(tcp->th_sport));
		printf("Dst Port: %d\n", ntohs(tcp->th_dport));

		if (packet_check > header->caplen) printf("File Type ERROR");
		else if (packet_check == header->caplen) printf("There is no data");
		else {
			printf("Data: ");
			for(int i=0; i<10; i++) {
				if (packet[i] == 0) break ; 
				printf("%02x ", packet[i]);
			}
		}
		printf("\n");
	};

	pcap_close(pcap);
}
